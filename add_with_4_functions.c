#include <stdio.h>
int input()
{
	int num1;
	printf("Enter a number\n");
	scanf("%d",&num1);
	return num1;
}
int result_of(int num1, int num2)
{
	int sum;
	sum = num1+num2;
	return sum;
}
void output(int num1, int num2, int num3)
{
	printf("Sum of %d + %d is %d\n",num1,num2,num3);
}
int main()
{
	int a,b,c;
	 a=input();
	b=input();
	c=result_of(a,b);
	output(a,b,c);
	return 0;
}